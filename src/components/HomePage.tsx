import React from "react";
import ReactMarkdown from "react-markdown";
import Box from "@material-ui/core/Box";
import homePageContent from "../websiteContent/homePageContent.md";
import homePageFooter from "../websiteContent/homePageFooter.md";
import { Typography } from "@material-ui/core";
import { ReactMarkdownUtils } from "App/components/reactMarkdown";

const HomePage: React.FunctionComponent = () => (
    <Box paddingBottom={1}>
        <Box textAlign="center">
            <Typography variant="h1" component="h1">
                Universal Integration Knowledge Base
            </Typography>
        </Box>
        <ReactMarkdown
            renderers={{
                paragraph: ReactMarkdownUtils.MDRenderersCustom.paragraph,
                heading: ReactMarkdownUtils.MDRenderersCustom.heading,
                link: ReactMarkdownUtils.MDRenderersCustom.internalLink,
            }}
            source={homePageContent}
        />
        <ReactMarkdown renderers={ReactMarkdownUtils.MDRenderObject} source={homePageFooter} />
    </Box>
);
export default HomePage;
