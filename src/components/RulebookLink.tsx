import React from "react";
import { NavLink } from "react-router-dom";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import { IconButton } from "@material-ui/core";
import Box from "@material-ui/core/Box/Box";

interface IProps {
    path: string;
}
export const RulebookLink: React.FunctionComponent<IProps> = (props) => (
    <Box style={{ display: "inline-flex", alignItems: "center" }}>
        {props.children}{" "}
        <NavLink to={props.path}>
            <IconButton>
                <HelpOutlineIcon />
            </IconButton>
        </NavLink>
    </Box>
);
