import SearchIcon from "@material-ui/icons/Search";
import React, { useEffect, useState } from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { createStyles, Theme } from "@material-ui/core";
import { fade } from "@material-ui/core/styles";
import { IGlobalState } from "App/appstate/store";
import AutoComplete from "./atom/Autocomplete";
import { connect } from "react-redux";
import { actions, selectors } from "App/modules/instruments/ducks";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        search: {
            position: "relative",
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            "&:hover": {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: "100%",
            [theme.breakpoints.up("sm")]: {
                marginLeft: theme.spacing(1),
                width: "auto",
            },
        },
        searchIcon: {
            width: theme.spacing(7),
            height: "100%",
            position: "absolute",
            pointerEvents: "none",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        },
    })
);

const mapStateToProps = (state: IGlobalState) => ({
    selectedInstrument: selectors.getSelectedInstrumentId(state),
    instruments: selectors.getInstruments(),
});
const mapDispatchToProps = {
    selectInstrument: actions.selectInstrument,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

interface ISearchBarProps extends StateProps, DispatchProps {}

const SearchBar: React.FunctionComponent<ISearchBarProps> = ({ selectInstrument, selectedInstrument, instruments }) => {
    const classes = useStyles();

    const [list, setList] = useState<string[]>([]);
    useEffect(() => {
        const newList: string[] = [];
        instruments.forEach((instrument) => newList.push(instrument.fullName));
        setList(newList);
    }, [instruments]);

    return (
        <div className={classes.search}>
            <div className={classes.searchIcon}>
                <SearchIcon />
            </div>
            <AutoComplete list={list} onSelection={selectInstrument} selected={selectedInstrument} />
        </div>
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
