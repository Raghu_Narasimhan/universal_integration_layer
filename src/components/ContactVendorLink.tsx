import React from "react";
import Typography from "@material-ui/core/Typography/Typography";
import Link from "@material-ui/core/Link";
import { IGlobalState } from "App/appstate/store";
import { selectors } from "App/modules/instruments/ducks";
import { connect } from "react-redux";

interface IContactVendorLinkOwnProps {
    vendorName: string;
}

const mapStateToProps = (state: IGlobalState, ownProps: IContactVendorLinkOwnProps) => ({
    vendor: ownProps.vendorName ? selectors.getVendorByName(ownProps.vendorName) : undefined,
});

const ContactVendorLinkView: React.FC<ReturnType<typeof mapStateToProps> & IContactVendorLinkOwnProps> = ({ vendor }) => {
    if (!vendor) {
        return null;
    }
    const vendorLink = vendor.contact.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/) ? `mailto:${vendor.contact}` : vendor.contact;
    return <Typography variant="body2">Contact: <Link target="_blank" href={vendorLink}>{vendor.contact}</Link></Typography>;
}

export default connect(mapStateToProps)(ContactVendorLinkView)
