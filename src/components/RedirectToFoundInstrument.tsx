import React from "react";
import { IGlobalState } from "App/appstate/store";
import { selectors } from "App/modules/instruments/ducks";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { SHOW_INSTRUMENT_ROUTE } from "App/App";

const mapStateToProps = (state: IGlobalState, ownProps: IFoundInstrumentsOwnProps) => ({
    selectedInstrument: selectors.getSelectedInstrumentId(state),
});
const mapDispatchToProps = {};

interface IFoundInstrumentsOwnProps {}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

interface IFoundInstrumentsProps extends StateProps, DispatchProps, IFoundInstrumentsOwnProps {}

const RedirectToFoundInstrument: React.FunctionComponent<IFoundInstrumentsProps> = ({ selectedInstrument }) => {
    if (selectedInstrument) {
        return <Redirect to={SHOW_INSTRUMENT_ROUTE + "/" + selectedInstrument} />;
    }
    return null;
};

export default connect(mapStateToProps, mapDispatchToProps)(RedirectToFoundInstrument);
