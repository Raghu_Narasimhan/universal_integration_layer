import { InternalLinkRenderer, LinkRenderer } from "App/components/reactMarkdown/Links";
import { Heading } from "App/components/reactMarkdown/Header";
import { Paragraph } from "App/components/reactMarkdown/Paragraph";
import CodeBlock from "App/components/reactMarkdown/Code";
import { MDListItemRender, MDListRender } from "App/components/reactMarkdown/MDListRender";

const MDRenderObject = {
    link: LinkRenderer,
    heading: Heading,
    paragraph: Paragraph,
    code: CodeBlock,
    list: MDListRender,
    listItem: MDListItemRender,
};

const MDRenderersCustom = {
    link: LinkRenderer,
    heading: Heading,
    paragraph: Paragraph,
    internalLink: InternalLinkRenderer,
    code: CodeBlock,
    list: MDListRender,
    listItem: MDListItemRender,
};

export type MDRenderObject = typeof MDRenderObject;

export const ReactMarkdownUtils = {
    MDRenderObject,
    MDRenderersCustom,
};
