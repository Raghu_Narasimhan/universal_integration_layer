import React from "react";
import { Typography } from "@material-ui/core";

export const Heading: React.FunctionComponent<any> = (props) => (
    <Typography variant={"h" + props.level} {...props} style={{ paddingTop: 20 }}>
        {props.children}
    </Typography>
);
