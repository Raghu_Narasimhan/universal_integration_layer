import React from "react";
import { Typography } from "@material-ui/core";

export const Paragraph: React.FunctionComponent<any> = ({ href, ...props }) => <Typography component="p" {...props} style={{
    marginBlockStart: "1em",
    marginBlockEnd: "1em",
}}/>;
