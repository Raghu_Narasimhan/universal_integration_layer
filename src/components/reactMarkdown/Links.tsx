import React from "react";
import { Link } from "react-router-dom";

export const LinkRenderer: React.FunctionComponent<any> = (props) => {
    let target = "_blank";
    try {
        const targetHref = new URL(props.href);
        target = targetHref.host.length === 0 ? "_self" : "_blank";
    } catch (e) { } // malformed URL

    return (
        <a href={props.href} target={target} rel="noopener noreferrer" {...props}>
            {props.children}
        </a>
    );
};

export const InternalLinkRenderer: React.FunctionComponent<any> = ({ href, ...props }) => <Link to={href} {...props} />;
