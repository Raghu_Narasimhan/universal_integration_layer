import { createBrowserHistory, LocationDescriptorObject, Path } from "history";

const AppHistory = createBrowserHistory({
    basename: process.env.PUBLIC_URL || "",
});
const prevHistoryPush = AppHistory.push;
let lastLocation = AppHistory.location;

AppHistory.listen((location) => {
    lastLocation = location;
});

/**
 * @ImplNote:
 * The following method is a monkey patch to overcome the inability of native history push method
 * to correctly avoid pushing equal location states. This in fact allows us to:
 *  1. be in one location,
 *  2. click two times in the same nav link and then
 *  3. click just once on the back button
 *  4. go to the previous location
 *  Previously, one had to click two times on the back button.
 */
AppHistory.push = (pathname: Path | LocationDescriptorObject<any>, state: {} = {}) => {
    if (
        lastLocation === null ||
        pathname !== lastLocation.pathname + lastLocation.search + lastLocation.hash ||
        JSON.stringify(state) !== JSON.stringify(lastLocation.state)
    ) {
        prevHistoryPush(pathname as string, state);
    }
};

export default AppHistory;
