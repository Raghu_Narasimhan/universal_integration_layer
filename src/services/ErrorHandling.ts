const ErrorHandling = async (f: () => void, onError: (err: string) => void): Promise<boolean> => {
    try {
        await f();
        return true;
    } catch (error) {
        console.error(error);
        if (error.message) {
            onError(error.message);
        } else {
            onError(JSON.stringify(error));
        }
        return false;
    }
};

export default ErrorHandling;
