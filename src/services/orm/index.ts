import { RepoBuilder } from "./EntityRepository";
import { ORMUtils } from "./ORMUtils";
import manager from "./RepositoryManager";

/**
 * Types
 */
export interface IObjectWithId {
    id: string;
}

export interface IDictionary {
    [name: string]: any;
}

export interface IndexedChildren {
    [id: string]: boolean;
}
export interface IOneToManyIndex {
    [id: string]: IndexedChildren;
}

export interface ITypeByID<T extends IObjectWithId> {
    [id: string]: T;
}

export interface IORMState {
    maps: {
        [entityName: string]: ITypeByID<IObjectWithId>;
    };
    index: {
        [relationTable: string]: IOneToManyIndex;
    };
}

export { RepoBuilder, manager, ORMUtils };
