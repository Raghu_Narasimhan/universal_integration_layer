import { IObjectWithId, IOneToManyIndex, IORMState, ITypeByID, IDictionary } from ".";

/**
 * maps
 */
const addToMap = <T extends IORMState>(state: T, entityName: string, entityObject: IObjectWithId): T => {
    return {
        ...state,
        maps: {
            ...state.maps,
            [entityName]: {
                ...state.maps[entityName],
                [entityObject.id]: entityObject,
            },
        },
    };
};

const removeFromMap = <T extends IORMState>(state: T, entityName: string, id: string): T => {
    const newMap = { ...state.maps[entityName] };
    delete newMap[id];
    return {
        ...state,
        maps: {
            ...state.maps,
            [entityName]: newMap,
        },
    };
};

/**
 * indexing
 */
const getFromIndex = <T extends IObjectWithId>(
    indexedEntityCollection: IOneToManyIndex,
    indexEntityKey: string,
    mappedEntityCollection: ITypeByID<T>
): T[] => {
    return indexedEntityCollection[indexEntityKey]
        ? Object.keys(indexedEntityCollection[indexEntityKey]).map((id) => mappedEntityCollection[id])
        : [];
};

const addToIndex = <T extends IORMState>(
    state: T,
    relationTable: string,
    parentPrimaryKeyValue: string,
    entityPrimaryKeyValue: string
): T => {
    const relationTableMap: IOneToManyIndex = state.index[relationTable] || {};
    const newIndexValues = relationTableMap[parentPrimaryKeyValue]
        ? { ...relationTableMap[parentPrimaryKeyValue], [entityPrimaryKeyValue]: true }
        : { [entityPrimaryKeyValue]: true };
    return {
        ...state,
        index: {
            ...state.index,
            [relationTable]: {
                ...relationTableMap, // pre-existing state is inserted here
                [parentPrimaryKeyValue]: newIndexValues,
            },
        },
    };
};

const removeValueFromIndex = <T extends IORMState>(
    state: T,
    relationTable: string,
    parentPrimaryKeyValue: string,
    entityPrimaryKeyValue: string
): T => {
    const values = { ...state.index[relationTable][parentPrimaryKeyValue] };
    if (values) {
        delete values[entityPrimaryKeyValue];
    }
    return {
        ...state,
        index: {
            ...state.index,
            [relationTable]: {
                ...state.index[relationTable],
                [parentPrimaryKeyValue]: values,
            },
        },
    };
};

const removeKeyFromIndex = <T extends IORMState>(state: T, relationTable: string, parentPrimaryKeyValue: string): T => {
    const index = { ...state.index[relationTable] };
    if (index) {
        delete index[parentPrimaryKeyValue];
    }
    return {
        ...state,
        index: {
            ...state.index,
            [relationTable]: index,
        },
    };
};

/**
 * addToMapByPrimaryKey()
 * will create a map with the entity primary key as key, as entities could have different primary keys
 */
const addToMapByPrimaryKey = <S extends IORMState, O extends IDictionary>(
    state: S,
    entityName: string,
    primaryKey: string,
    entityObject: O
): S => {
    return {
        ...state,
        maps: {
            ...state.maps,
            [entityName]: {
                ...state.maps[entityName],
                [entityObject[primaryKey]]: entityObject,
            },
        },
    };
};

export const ORMUtils = {
    addToMap,
    addToMapByPrimaryKey,
    addToIndex,
    removeValueFromIndex,
    removeKeyFromIndex,
    removeFromMap,
    getFromIndex,
};
