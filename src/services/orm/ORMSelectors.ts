import { IORMState, IDictionary } from ".";

/**
 * Types
 */
type GetAll = <S extends IORMState>(state: S, table: string) => IDictionary[];
type GetById = <S extends IORMState>(state: S, table: string, parentId: string) => IDictionary | undefined;

/**
 * Selectors
 */
const getAll: GetAll = (state, table) => Object.values(state.maps[table]);
const getById: GetById = (state, table, parentId) => state.maps[table][parentId];

export const ORMSelectors = {
    getAll,
    getById,
};
