import { Action, Reducer as ReducerFromRedux } from "redux";

// Redux Actions
export interface ISimpleAction<TType = Action> {
    type: TType;
}

export interface IPayloadAction<TType, TPayload> extends ISimpleAction<TType> {
    payload: TPayload;
}

// Reducer
export type Reducer<StateI, ActionI extends Action<any>> = ReducerFromRedux<StateI, ActionI>;
export interface IDictionary {
    [name: string]: any;
}
