import React from "react";
import { Provider } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import CssBaseline from "@material-ui/core/CssBaseline/CssBaseline";
import Box from "@material-ui/core/Box/Box";
import { store } from "App/appstate/store";
import AppHistory from "App/services/appHistory";
import Header from "./components/Header";
import HomePage from "./components/HomePage";
import SearchBar from "./components/SearchBar";
import Notfound from "./components/NotFound";
import ShowInstrument from "App/modules/instruments/components/ShowInstrument";
import RedirectToFoundInstrument from "App/components/RedirectToFoundInstrument";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { defaultMaterialTheme } from "App/services/styles";
import ListPage, { LIST_ROUTE } from "App/components/ListPage";
import BootstrapContainerGrid from "App/components/BootstrapLikeContainer";
import Rulebook from "App/components/Rulebook";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { createStyles, Theme } from "@material-ui/core";

export const SHOW_INSTRUMENT_ROUTE = "/show-instrument";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        app: {
            "& img": {
                maxWidth: "100%",
            },
        },
    })
);

const App: React.FunctionComponent = () => {
    const classes = useStyles();
    return (
        <Provider store={store}>
            <MuiThemeProvider theme={defaultMaterialTheme}>
                <ConnectedRouter history={AppHistory}>
                    <CssBaseline />
                    <Header>
                        <SearchBar />
                    </Header>

                    <RedirectToFoundInstrument />

                    <BootstrapContainerGrid>
                        <Box p={2} className={classes.app}>
                            <Switch>
                                <Route exact path={"/"} component={HomePage} />
                                <Route exact path={LIST_ROUTE} component={ListPage} />
                                <Route path={SHOW_INSTRUMENT_ROUTE + "/:instrumentName"} component={ShowInstrument} />
                                <Route path={"/rulebook"} component={Rulebook} />
                                <Route component={Notfound} />
                            </Switch>
                        </Box>
                    </BootstrapContainerGrid>
                </ConnectedRouter>
            </MuiThemeProvider>
        </Provider>
    );
};

export default App;
