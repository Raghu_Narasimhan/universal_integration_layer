##### Bringing transparency and structured documentation on how to interface with instruments in the lab

We are currently working with Pistoia Alliance (https://www.pistoiaalliance.org/) on new version of the knowledge base, and a possible relocation of the site, so please stay tuned! Updates coming soon!


#### How to use this app

-   Use the search field to select an instrument

-   Go to the [instrument list](/list) and select the instrument you would like to learn about

-   See the [rulebook](/rulebook) to understand the vocabulary to represent instrument integrations

-   Watch this video walkthrough (https://www.loom.com/share/ff7c0608bb204be48429f87cc8733802) to learn more

