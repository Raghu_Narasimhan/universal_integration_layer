Universal Integration Layer Rulebook
====================================

***An initiative by Roche pREDi Discovery Informatics***

## Introduction

This rulebook has stemmed out of a project supported by Roche. It was found that the integration of laboratory devices is very time-consuming. Also, it was observed that the actual software implementation is a rather low effort compared to finding the information on how different capabilities are implemented. The problems for the information gathering can be summarised as:

* Different companies are not talking the same language when it comes to device integration.

* Integration standards such as SiLA or OPC are slow in adoption by vendors, which means one can not rely on them at this point in time.

* It is very difficult to even estimate the effort an device integration will take as it’s not even clear what capabilities are provided and which not

In this document we propose a Universal Integration Layer (UIL) Rulebook that should alleviate these problems by:

1. Defining standard vocabulary about device integration
1. Defining a standard functionality set that every device should document
1. Document different device types and how the standard functionality set is implemented

## Table of Contents

- [Universal Integration Layer Rulebook](#universal-integration-layer-rulebook)
  * [Introduction](#introduction)
  * [Table of Contents](#table-of-contents)
  * [Conformance Language](#conformance-language)
  * [System Integration Levels](#system-integration-levels)
  * [Standard Vocabulary](#standard-vocabulary)
    + [Introduction](#introduction-1)
    + [Functionality Set](#functionality-set)
      - [Availability](#availability)
        * [Not supported](#not-supported)
        * [Not applicable](#not-applicable)
        * [Applicable](#applicable)
      - [Dependencies](#dependencies)
    + [Integration Interface](#integration-interface)
      - [Interface Types](#interface-types)
        * [SiLA 2](#sila-2)
        * [OPC-UA](#opc-ua)
        * [REST](#rest)
        * [Serial](#serial)
        * [TCP](#tcp)
        * [DLL](#dll)
        * [COM](#com)
        * [CSV](#csv)
        * [XML](#xml)
        * [BMP](#bmp)
        * [TXT](#txt)
        * [Excel](#excel)
    + [Physical Interface](#physical-interface)
      - [RS232](#rs232)
      - [Ethernet](#ethernet)
      - [USB](#usb)
      - [Wifi](#wifi)
    + [Category: Device](#category--device)
      - [Vocabulary](#vocabulary)
          + [Device Status](#device-status)
          + [Device Simulation Setting](#device-simulation-setting)
        * [Device Error](#device-error)
          + [Device Error Resolution](#device-error-resolution)
          + [Device Error Retrieval](#device-error-retrieval)
        * [Device Configuration](#device-configuration)
          + [Device Connection](#device-connection)
          + [Device Initialization](#device-initialization)
          + [Device Termination](#device-termination)
      - [Functionality Set](#functionality-set-1)
    + [Category: Method](#category--method)
      - [Vocabulary](#vocabulary-1)
        * [Method Management](#method-management)
          + [Method Logic](#method-logic)
          + [Method Download](#method-download)
          + [Method Upload](#method-upload)
          + [Method Removal](#method-removal)
          + [Method Creation](#method-creation)
          + [Method Discovery](#method-discovery)
        * [Method Data Management](#method-data-management)
          + [Method Input Setting](#method-input-setting)
          + [Method Output Retrieval](#method-output-retrieval)
        * [Method Control](#method-control)
          + [Method Selection](#method-selection)
          + [Default Method](#default-method)
          + [Method Start](#method-start)
          + [Method Pausing And Resuming](#method-pausing-and-resuming)
          + [Method Cancellation](#method-cancellation)
        * [Method Monitoring](#method-monitoring)
          + [Method Progress Retrieval](#method-progress-retrieval)
          + [Method Status Retrieval](#method-status-retrieval)
        * [Method Error](#method-error)
          + [Method Error Resolution](#method-error-resolution)
          + [Method Error Retrieval](#method-error-retrieval)
      - [Functionality Set](#functionality-set-2)
  * [Device Documentation](#device-documentation)
    + [Structure](#structure)
      - [Description](#description)
  * [Integration Documentation](#integration-documentation)
      - [Minimum requirements](#minimum-requirements)
      - [Support Points](#support-points)
      - [Version](#version)
      - [Functionality Set Support](#functionality-set-support)
        * [Functionality](#functionality)
        * [Implementation](#implementation)
          + [Interface Information](#interface-information)
      - [Schema](#schema)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>
## Conformance Language

Special terms are defined at their point of introduction in the text.
For example:

[Definition: Term] a **Term** is something used with a special meaning. The definition is labeled as such and the **Term** it defines is displayed in boldface. The end of the definition is not specially marked in the displayed or printed text. Uses of defined Terms are links to their definitions, set off with middle dots, for instance ·Term·.

We use the following keywords to indicate requirements levels:

* MUST
* MUST NOT
* SHOULD
* SHOULD NOT
* MAY

These are to be interpreted as described in [RFC2119](https://www.ietf.org/rfc/rfc2119.txt).

## System Integration Levels

Three different system integration levels are defined to describe different range of functionality that one might want to access:

* Shallow: Only concerned with behaviour that is generalisable across all devices and doesn’t go into specific functionality
* Deep: Is concerned with device specific behaviour but on the abstraction level the device vendor wants to expose
* Complete: Gives full control of any functionality that can be provided

An overview of the different levels and their properties is given:

| | Example | Application | Caveats |
| --- | --- | --- | --- |
| Shallow | Start method X | Lab dashboard, Workflow orchestrator | Limited control, abstract behaviour |
| Deep | Dispense 50 ul into all wells | Safety systems, Custom automation cell | device-specific |
| Complete | Rotate servo motor | device vendor software | Tedious, limited value addable |

For the Universal Integration Layer the primary focus is on the Shallow Layer, but the Deep Layer will also be considered for the device documentation. The Complete layer will be ignored.

## Standard Vocabulary

### Introduction

The goal of the UIL Standard Vocabulary is to define a complete device-agnostic set of terms that can describe the functionality needed from device integration in the majority of use cases in the life science laboratory.

For now, this vocabulary is not concerned with the Deep Level, so e.g. actions like dispensing, shaking etc. are not being standardised in these efforts.

### Functionality Set
For each category in the standard vocabulary, a set of functionality called **Functionality Set** is defined whose behaviour needs to be documented for each device. This is a subset of each standard vocabulary category.

#### Availability
A functionality is **available** if it can be used by any software means, i.e. another computer system can use this functionality on the device.

If the functionality is not available on this device it MUST be categorised either as ·Not supported· or ·Not applicable· (N/A).

##### Not supported
The functionality could be in theory supported but would need effort from the vendors side.

##### Not applicable
The functionality is not possible to be implemented on this device.

##### Applicable
As a corollary to ·Not applicable·, a functionality is **applicable** if it is either ·available· or ·Not supported·.

#### Dependencies

For each functionality there may be a list of functionalities that it depends on to be applicable on the device. This means that this functionality only has to be documented if all functionalities listed in dependencies are ·applicable·.


### Integration Interface
This describes the interface another piece of software can use to use the functionality described. The interface MUST be described if the functionality is available on the device. Otherwise “N/A” MUST be used.

#### Interface Types

**Standard**

##### [SiLA 2](https://sila-standard.com/standards/)
A standard in laboratory automation. Focusing on connectivity, discoverability of device capabilities, and control.

##### [OPC-UA](https://opcfoundation.org/developer-tools/specifications-unified-architecture)
A machine to machine communication protocol for industrial automation. Focus on communicating with industrial equipment and systems for data collection and control.

**Communication Protocols**

##### [REST](https://en.wikipedia.org/wiki/Representational_state_transfer)
Representational state transfer (REST) is a software architectural style that defines a set of constraints to be used for creating Web services.


##### [Serial](https://en.wikipedia.org/wiki/Serial_communication)
Specifies the interface to be any serial communication protocol with specifics hidden in the [Physical Interface](#Physical-Interface).

##### [TCP](https://en.wikipedia.org/wiki/Transmission_Control_Protocol)
An interface used by using the transmission control protocol (TCP) directly without a higher level protocols such HTTP.


**Library**

##### [DLL](https://support.microsoft.com/en-gb/help/815065/what-is-a-dll)
Dynamic-link library (DLL) is Microsoft's implementation of the shared library concept in the Microsoft Windows and OS/2 operating systems. These libraries usually have the file extension DLL, OCX (for libraries containing ActiveX controls), or DRV (for legacy system drivers)

##### [COM](https://docs.microsoft.com/en-us/windows/win32/com/component-object-model--com--portal)
The component object model (COM) binary-interface standard developed by Microsoft.

**File**

##### [CSV](https://tools.ietf.org/html/rfc4180)
Delimiter separated file with N rows of data structured by column names found in coverage. The delimiter can be either {Comma-separated or Semicolon separated}

##### [XML](https://www.w3.org/TR/xml/)
A text document structured using the Extensible Markup Language (XML) format. The files structure SHOULD be defined by a schema.

##### [BMP](https://en.wikipedia.org/wiki/Bitmap)
An image file format which stands for **bitmap**. The image is stored as a spacially mapped array of bits.

##### TXT
A text document with a (.txt) file extension which may have any structure defined

##### Excel
A binary file format which supports storing Excel spreadsheets. Possible file extensions include .xls, (.xlsx, .xlsm, .xlsb - Excel 2007)

### Physical Interface
A [Device](#category:-device) can have one or more physical interfaces which refer to the layer that deals with electronic circuit transmission technologies of a network, can be wired or wire-less.

#### RS232
RS232 is a standard for serial communication. In this document RS232 represents all range of physical connections which MAY use RS232 serial. For example a DE-9M connector.

#### [Ethernet](https://en.wikipedia.org/wiki/Ethernet_physical_layer)
When refering to Ethernet, what is meant is the Ethernet physical layer and specifically the [8P8C Connector](https://en.wikipedia.org/wiki/Modular_connector#8P8C).

#### [USB](https://en.wikipedia.org/wiki/USB)
We refer to all four generations of Universal Serial Bus (USB) specifications.

#### Wifi
The family of wireless networking technology based on IEEE 802.11 standards. Especially for Wifi it is vital to know how to connect the [Device](#category:-device) to a certain network.

### Category: Device 
A **Device** represents any physical entity that has the capability of running specific operations and methods.

*Potential synonyms: Instrument*

#### Vocabulary 

###### Device Status 
The representation of the operational state of the device covering all. Example states include: error, running, loading, stopped.

###### Device Simulation Setting
**Simulation**: The ability to interact with the device such that the interaction models the real world behaviour without affecting the real world. This ability is usually used for the purpose of applications in non-productive contexts (such as training, testing, development)

##### Device Error 
The detailed representation of a cause or reason for the device to be in an error state.
**Error**: Any fault or failure to successfully complete an action on the device. This could be an hardware fault, execution error or due to some other reason. <a name="defn-error"></a>

###### Device Error Resolution 
The act of clearing or resolving the [error](#defn-error) in the device. This could be simply clearing an error in a 
register, or acknowledging to the system that an error has been retrieved such that the device can proceed with 
execution of further actions.
 
###### Device Error Retrieval 
The act of retrieving an [error](#defn-error) that has occurred in the device. This may usually be the previously known 
error, but could also require retrieval of errors based on some parameter.

##### Device Configuration 
**Configuration** represents the creation or initialization and termination of some resource used to interface with the device. In the case for a DLL interface to the device this would be the construction of the device and destruction. In the case for [RS-232 Serial](#rs232) this would involve initial set of commands and operations required to initiate communication with the device channel/interface. Note that even though physical interfaces are defined on a device level, the connection functionality might depend on the integration - for example if it is automatic or has to be established by a given software input.

###### Device Connection
The act of configuration of the interface/channel or to interact with the interface or channel. Only necessary for certain interfaces.

###### Device Initialization
The act of setting the device/device to a state whereby it is ready to operate/work. This consists of any steps and operations which must be issued to the device before further operations/commands can be executed. For example the homing of a robotic arm.

###### Device Termination
The act of closing down a resource, terminating usage of an interface to an device.
In some cases devices require to be set back to a given state for further usage at a later stage
e.g. manual usage, or via another interface.

#### Functionality Set 

| Functionality | Dependencies |
| --- | --- |
| Device Status | |
| Device Error Retrieval | |
| Device Error Resolution | <ul><li>Device Error Retrieval</li></ul>|
| Device Simulation Setting | |
| Device Connection | |
| Device Initialization | |
| Device Termination | |

### Category: Method
A **Method** represents any action that can be performed on an device.

*Potential synonyms: Program, protocol, script, actions, commands, instructions*

#### Vocabulary

##### Method Management
The act of retrieving and changing the state of available ·Methods·. **Method management** only makes sense if the ·Methods· an device provides are changeable by their nature.

###### Method Logic
Content that describes the logic of execution for example in the form of a programming language of a certain ·Methods·. The **Method Logic** always MUST follow a known structure.

###### Method Download
The act of retrieving [Method Logic](#method-logic) stored on the device.

###### Method Upload
The act of uploading a [Method Logic](#method-logic) . A [Method](#category--method) that is uploaded MUST be available for [Method Control](#method-control) later on.

###### Method Removal
The act of removing a [Method Logic](#method-logic) stored on the device

###### Method Creation
The act of creating a [Method Logic](#method-logic) to be executed on the device.

###### Method Discovery
The act of finding all available [Method](#category--method)'s. As examples, this can just be files with a certain format in a certain folder, or automatic procedures such as e.g. SiLA Discovery.

##### Method Data Management

###### Method Input Setting
 A [Method](#category--method) MAY accept inputs to perform specific actions with variable settings, such as for e.g. the case of performing a certain action for X amount of time. **Method Input Setting** is the act of setting these inputs.

*Potential synonyms: Variables, settings, parameters*

###### Method Output Retrieval
A [Method](#category--method) MAY produces output that is of interest to a user of the device. **Method Output Retrieval** is the act of retrieving these outputs.

*Potential synonyms: Result, Measurement, Return Value*

##### Method Control
The act of controlling the state of execution of any available [Method](#category--method).

###### Method Selection
**Method Selection** is the act of selecting a  [Method](#category--method) to be executed with a [Method Start](#method-start).

###### Default Method
**Default Method** is the [Method](#category--method) to be used if no other [Method](#category--method) is selected for execution. It is relevant to define device purpose or main usage, or if the device only has one action.

###### Method Start
Starting the execution of a [Method](#category--method).

###### Method Pausing And Resuming
The [Method](#category--method) MAY be paused and its execution MAY also be resumed without any loss of data or operations.

###### Method Cancellation
The act of aborting/cancelling/stopping a [Method](#category--method) being executed. 

##### Method Monitoring

###### Method Progress Retrieval
The act of retrieving the progress of the [Method](#category--method) execution. As an example, this could be the combination of percentage-wise completion, estimated remaining time or if it has already started.

###### Method Status Retrieval
The act of retrieving the state of the [Method](#category--method). As an example, this could be the combination of retrieving information about the current state of the method upon execution.

##### Method Error 
The detailed representation of a cause or reason for the method to be in an [error](#defn-error) state.

###### Method Error Resolution 
The act of clearing or resolving the [error](#defn-error) in the method. This is similar to [error resolution](#error-resolution) at a device level.
 
###### Method Error Retrieval 
The act of retrieving an [error](#defn-error) that has occurred in the device. This may usually be the previously known 
error, but could also require retrieval of errors based on some parameter.

#### Functionality Set 

| Functionality | Dependencies |
| --- | --- |
| Method Logic | |
| Method Download | <ul><li>Method Start</li><li>Method Selection</li></ul> |
| Method Upload | <ul><li>Method Start</li><li>Method Selection</li></ul> |
| Method Removal | <ul><li>Method Selection</li></ul> |
| Method Creation | <ul><li>Method Download</li></ul> |
| Method Discovery | <ul><li>Method Start</li><li>Method Selection</li></ul> |
| Method Input Setting | <ul><li>Method Start</li></ul> |
| Method Output Retrieval | <ul><li>Method Start</li></ul> |
| Method Selection | <ul><li>Method Start</li></ul> |
| Default Method | <ul><li>Method Start</li></ul> |
| Method Start | |
| Method Pausing And Resuming | <ul><li>Method Start</li></ul> |
| Method Cancellation | <ul><li>Method Start</li></ul> |
| Method Progress Retrieval | <ul><li>Method Start</li></ul> |
| Method Status Retrieval | <ul><li>Method Start</li></ul> |
| Method Error Resolution |<ul><li>Method Start</li></ul> |
| Method Error Retrieval |<ul><li>Method Start</li></ul> |

## Device Documentation

The target of the device documentation is that every developer can get up to speed fairly quickly to develop his own integration in whatever environment he chooses.

### Structure

Every device MUST describe the following sections:

#### Description
A high level description of the device, especially geared towards the context of integration.

## Integration Documentation
**Integration**: An integration is defined as any component which exposes 1-to-N interfaces to interact with the device
either to obtain data, control, or configure the device.

The target of the integration documentation is to keep track of relevant information accessible from one central place.
Categorizing by the [integration](#defn-integration) helps to draw the line between an the physical device
and any additional physical or software component which offers integration capabilities.

#### Minimum requirements
What components are needed to integrate the device (which library, piece of software, operating system etc.). In general the information provided should be software requirements.

#### Support Points
Vendors that can be asked to support with the integration of this device (including the device vendor itself)

#### Version
device version or version of interface which is being documented

#### Functionality Set Support
A full list of standard functionality, see below.

##### Functionality
The functionality should include the following:

    - The name of the functionality to be described, which can be either of the functionality listed in the categories
      above e.g. Method, Error, Measurement, Simulation.

    - The interfaces available for tha functionality. Which can be any of the ones listed in the [interface](#interface-types) section


##### Implementation
**Implementaion** Describes how the command can be triggered. Depending on the interface being used this would include snippets of code, commands (if RS233) or directories where files need to be written to. 

The information provided should only be a rough guideline and overview, with a link to further references with more information (e.g. API documentation, device manual).


###### Interface Information
**Interface Information** Describes the information which identifies where the interface is located. In the case for a DLL this would describe the name of the DLL to use. In the case for RS232 this would describe the physical location to the connection. In the case with SiLA this would describe the Fully Qualified Feature Identifier.

#### Schema

![Schema](../../public-assets/media/rulebook/uil_schema.png)