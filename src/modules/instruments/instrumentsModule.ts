import { Store } from "redux";
import { actions, selectors } from "App/modules/instruments/ducks";
import ErrorHandling from "App/services/ErrorHandling";
import { IIndex } from "App/modules/instruments/models/models";
import { uilApi } from "App/modules/instruments/services/UilApi";
import Utilities from "App/services/Utilities";

const initialize = (store: Store) => {
    ErrorHandling(
        async () => {
            const index: IIndex = await uilApi.getIndex();
            index.vendors.forEach((vendorIdx) => {
                store.dispatch(
                    actions.addVendor({
                        ...vendorIdx,
                        id: Utilities.createUUID(), // doesn't have ID
                    })
                );
            });
            index.instruments.forEach((instrumentIdx) => {
                const instrumentsIdx = selectors.getInstruments();
                const foundItem = instrumentsIdx.find((item) => item.fullName === instrumentIdx.fullName);
                if (foundItem) {
                    store.dispatch(
                        actions.updateInstrument({
                            ...foundItem,
                            ...instrumentIdx,
                        })
                    );
                } else {
                    store.dispatch(
                        actions.addInstrument({
                            ...instrumentIdx,
                            id: Utilities.createUUID(), // doesn't have ID
                        })
                    );
                }
            });
            index.integrations.forEach((integrationIdx) => {
                const integrationsIndex = selectors.getIntegrations();
                const foundItem = integrationsIndex.find((item) => item.id === integrationIdx.id);

                if (foundItem) {
                    store.dispatch(
                        actions.updateIntegration({
                            ...foundItem,
                            ...integrationIdx,
                        })
                    );
                } else {
                    store.dispatch(
                        actions.addIntegration({
                            ...integrationIdx,
                        })
                    );
                }
            });

            // update pivot table instrument <-> integration
            selectors.getInstruments().forEach((instrument) => {
                instrument.integrationIds.forEach((integrationId) => {
                    // check if integration with this integrationId exist
                    const integration = selectors.getIntegrationById(integrationId);
                    if (integration) {
                        store.dispatch(
                            actions.addInstrumentIntegration({
                                id: Utilities.createUUID(),
                                instrumentId: instrument.id,
                                integrationId,
                            })
                        );
                    } else {
                        console.error("Integration with ID " + integrationId + " was not found");
                    }
                });
            });
        },
        (err: any) => console.error(err)
    );
};

const InstrumentsModule = {
    initialize,
};
export default InstrumentsModule;
