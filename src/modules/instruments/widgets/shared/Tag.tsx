import Avatar from "@material-ui/core/Avatar";
import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { createStyles, Theme } from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import Box from "@material-ui/core/Box/Box";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "inline-flex",
            "& > *": {
                margin: theme.spacing(1),
            },
        },
        rounded: {
            color: "#fff",
            backgroundColor: green[500],
            whiteSpace: "nowrap",
            width: "auto",
            padding: 6,
            fontSize: "small",
            height: "auto",
            margin: "0 5px",
        },
    })
);

interface ITagProps {
    key?: string;
    label: string;
    classes?: string;
}

const Tag: React.FC<ITagProps> = ({ label, classes = "" }) => {
    const _classes = useStyles();
    return (
        <Box className={_classes.root}>
            <Avatar variant="rounded" className={`${classes} ${_classes.rounded}`}>
                {label}
            </Avatar>
        </Box>
    );
};

export default Tag;
