import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

const DetailComp: React.FunctionComponent<{
    el: Element;
    classes?: { label?: string; content?: string };
    title: string;
}> = ({ el, classes, title }) => (
    <Box p={1} style={{ display: "inline-block" }}>
        {title && (
            <Typography variant="h5" className={classes && classes.label} component="span">
                {title}:
            </Typography>
        )}
        <Typography variant="h6" className={classes && classes.content} component="span">
            {" " + el.textContent}
        </Typography>
    </Box>
);
export default DetailComp;
