import React, { useEffect, useState, ChangeEvent, MouseEvent } from "react";
import { IXmlToJsxProps, XmlToJsxChildrenGenerator } from "App/modules/xmlToJsxGen";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Utilities from "App/services/Utilities";
import { genericInstrumentStyles } from "App/modules/instruments/widgets/components/Instrument";
import ShowMarkdownFromXml from "App/modules/instruments/components/ShowMarkdownFromXml";
import makeStyles from "@material-ui/core/styles/makeStyles";
import FunctionalityRow, { FunctionalityInterface } from "./integration/FunctionalityRow";
import SupportedInstrument from "App/modules/instruments/widgets/components/integration/SupportedInstrument";
import Grid from "@material-ui/core/Grid/Grid";
import { fade } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles/createMuiTheme";
import { createStyles } from "@material-ui/core";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Paper from "@material-ui/core/Paper/Paper";
import IconButton from "@material-ui/core/IconButton";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Popper from "@material-ui/core/Popper";
import FormGroup from "@material-ui/core/FormGroup";
import Button from "@material-ui/core/Button";
import FilterIcon from "@material-ui/icons/FilterList";
import { RulebookLink } from "App/components/RulebookLink";

export const useGridStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "100%",
            boxShadow: "0px 1px 6px -2px rgba(0,0,0,0.2)",
        },
        headerRow: {
            backgroundColor: fade(theme.palette.common.black, 0.05),
            textAlign: "center",
        },
        bodyRows: {
            "&:nth-of-type(odd)": {
                backgroundColor: fade(theme.palette.background.default, 0.5),
            },
        },
        label: {
            fontWeight: "lighter",
        },
        gridItem: {
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            padding: 10,
        },
        border: {
            borderColor: fade(theme.palette.common.black, 0.1),
            borderStyle: "solid",
            borderTopWidth: 0.5,
            borderBottomWidth: 0.5,
            borderLeftWidth: 1,
            borderRightWidth: 1,
        },
        method: {
            minWidth: "100%",
            height: "100%",
            alignItems: "center",
            justifyContent: "center",
            textAlign: "center",
            display: "flex" as "flex",
            padding: 10,
            "&:nth-of-type(even)": {
                backgroundColor: fade(theme.palette.common.black, 0.05),
            },
        },
    })
);

interface IInterfaceFilterContext {
    registerInterface: (iface: string) => void;
    unregisterInterface: (iface: string) => void;
    isInterfaceDisplayed: (iface: string) => boolean;
}

interface IFilterableInterface {
    name: string;
    show: boolean;
}

export const InterfaceFilterContext = React.createContext<IInterfaceFilterContext>({
    registerInterface: () => {},
    unregisterInterface: () => {},
    isInterfaceDisplayed: () => true,
});

const Integration: React.FC<IXmlToJsxProps> = ({ content }) => {
    const classes = genericInstrumentStyles();
    const gridClasses = useGridStyles();
    const integrationDescription = Utilities.getDirectChild(content, "Description");
    const name = Utilities.getDirectChild(content, "Name");
    const functionalityEls = Utilities.getDirectChildren(content, "Functionality");
    const [functionalityTypes, setFunctionalityTypes] = useState<FunctionalityInterface[]>([]);

    const supportedInstrumentEls = Utilities.getDirectChildren(content, "SupportedInstrument");

    const [processed, setProcessed] = useState(false);
    const [interfaces, setInterfaces] = useState<IFilterableInterface[]>([]);
    const [filterAnchorEl, setFilterAnchorEl] = useState<HTMLButtonElement | null>(null);

    const handleFilterClick = (event: MouseEvent<HTMLButtonElement>) =>
        setFilterAnchorEl((prev) => (prev ? null : event.currentTarget));

    const handleClickAway = () => setFilterAnchorEl(null);

    const registerInterface = (iface: string) =>
        setInterfaces((prev) => {
            if (!prev.find((i) => i.name === iface)) {
                return [...prev, { name: iface, show: true }];
            }
            return prev;
        });

    const unregisterInterface = (iface: string) => setInterfaces((prev) => prev.filter((i) => i.name !== iface));

    const toggleInterface = (iface: string) => (e: ChangeEvent<HTMLInputElement>, checked: boolean) => {
        if (interfaces.find((i) => i.name === iface)) {
            setInterfaces((prev) => prev.map((i) => (i.name === iface ? { ...i, show: checked } : i)));
        }
    };

    const showAllInterfaces = () => setInterfaces((prev) => prev.map((i) => ({ ...i, show: true })));

    const isInterfaceDisplayed = (iface: string) => {
        const filteredIface = interfaces.find((i) => i.name === iface);
        if (filteredIface) {
            return filteredIface.show;
        }
        return false;
    };

    useEffect(() => {
        if (functionalityEls && !processed) {
            const newFunctionalityTypes: FunctionalityInterface[] = [];
            functionalityEls.forEach((element) => {
                const functionalityEls = Utilities.getDirectChildren(element as Element, "Type");
                const availabilityEl = Utilities.getDirectChild(element as Element, "Availability");
                const interfaces = Utilities.getDirectChildren(element as Element, "Interface");

                if (functionalityEls && availabilityEl && availabilityEl.textContent) {
                    const newFunctionality: FunctionalityInterface = {
                        methods: functionalityEls,
                        availability: availabilityEl.textContent,
                        interfaces,
                        functionalityEl: element,
                    };
                    newFunctionalityTypes.push(newFunctionality);
                }
            });
            setProcessed(true);
            setFunctionalityTypes(newFunctionalityTypes);
        }
    }, [functionalityEls, processed]);

    const filterOpen = Boolean(filterAnchorEl);
    const filterId = filterOpen ? "filter-popper" : undefined;

    return (
        <InterfaceFilterContext.Provider value={{ registerInterface, unregisterInterface, isInterfaceDisplayed }}>
            <Box>
                <RulebookLink path={"/rulebook#system-integration-levels"}>
                    <Typography variant="h4" className={classes.label}>
                        {name && name.textContent}
                    </Typography>
                </RulebookLink>

                <SupportedInstrument supportedInstrumentElements={supportedInstrumentEls} />

                {integrationDescription && (
                    <Box p={1}>
                        <ShowMarkdownFromXml content={integrationDescription.textContent} />
                    </Box>
                )}
            </Box>

            <XmlToJsxChildrenGenerator content={content} />

            <Grid container className={gridClasses.root}>
                <Grid container item xs={12} className={gridClasses.headerRow}>
                    <Grid item xs={12} md={3} lg={2}>
                        <Box className={gridClasses.gridItem}>
                            <RulebookLink path={"/rulebook#category-method"}>
                                <Typography variant="h6" className={classes.label}>
                                    Functionality
                                </Typography>
                            </RulebookLink>
                        </Box>
                    </Grid>
                    <Grid item xs={12} md={3} lg={2}>
                        <Box className={gridClasses.gridItem}>
                            <RulebookLink path={"/rulebook#availability"}>
                                <Typography variant="h6" className={classes.label}>
                                    Availability
                                </Typography>
                            </RulebookLink>
                        </Box>
                    </Grid>
                    <Grid item xs={12} md={6} lg={8}>
                        <Box className={gridClasses.gridItem}>
                            <Box display="flex" justifyContent="center" alignItems="center" position="relative">
                                <RulebookLink path={"/rulebook#functionality-set-support"}>
                                    <Typography variant="h6" className={classes.label}>
                                        Implementation
                                    </Typography>
                                </RulebookLink>
                                <Box position="absolute" top={0} bottom={0} right={0}>
                                    <IconButton onClick={handleFilterClick}>
                                        <FilterIcon />
                                    </IconButton>
                                </Box>
                            </Box>
                            <Popper id={filterId} open={filterOpen} anchorEl={filterAnchorEl} placement="bottom-end">
                                <ClickAwayListener onClickAway={handleClickAway}>
                                    <Box component={Paper} paddingY={1} paddingX={2}>
                                        <FormGroup>
                                            {interfaces.map((iface) => (
                                                <FormControlLabel
                                                    key={iface.name}
                                                    control={
                                                        <Checkbox
                                                            checked={iface.show}
                                                            onChange={toggleInterface(iface.name)}
                                                        />
                                                    }
                                                    label={iface.name}
                                                />
                                            ))}
                                        </FormGroup>
                                        <Button onClick={showAllInterfaces} fullWidth={true} size="small">
                                            show all
                                        </Button>
                                    </Box>
                                </ClickAwayListener>
                            </Popper>
                        </Box>
                    </Grid>
                </Grid>
                {functionalityTypes.map((functionality, index) => (
                    <FunctionalityRow key={index} functionality={functionality} />
                ))}
            </Grid>
        </InterfaceFilterContext.Provider>
    );
};
export default Integration;
