import React, { useEffect, useState } from "react";
import Utilities from "App/services/Utilities";
import makeStyles from "@material-ui/styles/makeStyles/makeStyles";
import Box from "@material-ui/core/Box/Box";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableRow from "@material-ui/core/TableRow/TableRow";
import Typography from "@material-ui/core/Typography/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import createStyles from "@material-ui/core/styles/createStyles";
import { Theme } from "@material-ui/core";
import Button from "@material-ui/core/Button/Button";
import { Link } from "react-router-dom";
import { SHOW_INSTRUMENT_ROUTE } from "App/App";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import TablePagination from "@material-ui/core/TablePagination/TablePagination";
import Paper from "@material-ui/core/Paper/Paper";
import TableSortLabel from "@material-ui/core/TableSortLabel/TableSortLabel";

const StyledTableCell = withStyles((theme: Theme) =>
    createStyles({
        head: {
            backgroundColor: theme.palette.common.white,
            color: theme.palette.common.black,
            fontWeight: "lighter",
            fontSize: 20,
        },
        body: {
            fontSize: 12,
        },
    })
)(TableCell);

const StyledTableRow = withStyles((theme: Theme) =>
    createStyles({
        root: {
            "&:nth-of-type(odd)": {
                backgroundColor: theme.palette.background.default,
            },
        },
    })
)(TableRow);

const useStyles = makeStyles({
    root: {
        width: "400px",
    },
    table: {
        minWidth: "100%",
        boxShadow: "0px 1px 6px -2px rgba(0,0,0,0.2)",
    },
    label: {
        fontWeight: "lighter",
    },
});

type Order = "asc" | "desc";

function desc<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort<T>(array: T[], cmp: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

function getSorting<K extends keyof any>(
    order: Order,
    orderBy: K
): (a: { [key in K]: number | string }, b: { [key in K]: number | string }) => number {
    return order === "desc" ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

type TableItem = { vendor: string; model: string };

const SupportedInstrument: React.FC<{ supportedInstrumentElements: Element[] }> = ({ supportedInstrumentElements }) => {
    const classes = useStyles();

    const [order, setOrder] = React.useState<Order>("asc");
    const orderBy: keyof TableItem = "model"; // no need for any other
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const [tableItems, setTableItems] = useState<TableItem[]>([]);
    useEffect(() => {
        const newTableItems: TableItem[] = [];

        supportedInstrumentElements.forEach((el) => {
            const vendor = Utilities.getDirectChild(el, "Vendor");
            const model = Utilities.getDirectChild(el, "Model");
            if (vendor && vendor.textContent && model && model.textContent) {
                const newItem: TableItem = { vendor: vendor.textContent, model: model.textContent };
                newTableItems.push(newItem);
            }
        });

        setTableItems(newTableItems);
    }, [supportedInstrumentElements]);

    return (
        <Box className={classes.root} paddingTop={1} paddingBottom={1}>
            <Typography variant="h6" style={{ fontWeight: "lighter" }}>
                Supported instruments
            </Typography>
            <Paper className={classes.table}>
                <Table aria-label="supported-instruments-table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Vendor</StyledTableCell>
                            <StyledTableCell sortDirection={orderBy === "model" ? order : false}>
                                <TableSortLabel
                                    active={orderBy === "model"}
                                    direction={order}
                                    onClick={() => {
                                        const isDesc = orderBy === "model" && order === "desc";
                                        setOrder(isDesc ? "asc" : "desc");
                                    }}
                                >
                                    Model
                                </TableSortLabel>
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tableItems &&
                            tableItems.length > 0 &&
                            stableSort(tableItems, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((item, index) => (
                                    <SupportedInstrumentRow key={index} vendor={item.vendor} model={item.model} />
                                ))}
                    </TableBody>
                </Table>
                <TablePagination
                    rowsPerPageOptions={[4, 6, 10, 25]}
                    component="div"
                    count={tableItems.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </Box>
    );
};
export default SupportedInstrument;

const SupportedInstrumentRow: React.FC<TableItem> = ({ vendor, model }) => {
    return (
        <StyledTableRow>
            <StyledTableCell component="th" scope="row">
                <Typography variant="body1">{vendor}</Typography>
            </StyledTableCell>
            <StyledTableCell>
                <Box
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "100%",
                        alignItems: "center",
                    }}
                >
                    <Typography variant="body1">{model}</Typography>
                    <Button
                        color="inherit"
                        style={{ textTransform: "unset", marginLeft: 10 }}
                        component={Link}
                        to={SHOW_INSTRUMENT_ROUTE + "/" + vendor + " " + model}
                    >
                        <NavigateNextIcon />
                    </Button>
                </Box>
            </StyledTableCell>
        </StyledTableRow>
    );
};
