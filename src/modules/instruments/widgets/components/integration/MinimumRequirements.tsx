import React from "react";
import { IXmlToJsxProps } from "App/modules/xmlToJsxGen";
import Typography from "@material-ui/core/Typography/Typography";
import Box from "@material-ui/core/Box/Box";
import ShowMarkdownFromXml from "App/modules/instruments/components/ShowMarkdownFromXml";
import { RulebookLink } from "App/components/RulebookLink";

const MinimumRequirements: React.FC<IXmlToJsxProps> = ({ content }) => (
    <Box paddingTop={1} paddingBottom={1}>
        <RulebookLink path={"/rulebook#minimum-requirements"}>
            <Typography variant="h6" style={{ fontWeight: "lighter" }}>
                Minimum requirements
            </Typography>
        </RulebookLink>
        <ShowMarkdownFromXml content={content.textContent} />
    </Box>
);
export default MinimumRequirements;
