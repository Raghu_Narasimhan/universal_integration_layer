import React from "react";
import { IXmlToJsxProps } from "App/modules/xmlToJsxGen";
import Typography from "@material-ui/core/Typography/Typography";
import Box from "@material-ui/core/Box/Box";
import ContactVendorLink from "App/components/ContactVendorLink";

const Vendor: React.FC<IXmlToJsxProps> = ({ content }) => {
    return (
        <Box paddingY={1}>
            <Typography variant="h6" style={{ fontWeight: "lighter" }}>
                Vendor
            </Typography>
            <Typography>{content.textContent}</Typography>
            {content.textContent && <ContactVendorLink vendorName={content.textContent}/>}
        </Box>
    );
};

export default Vendor;
