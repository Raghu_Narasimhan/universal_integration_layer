import React, { useContext, useEffect } from "react";
import { IXmlToJsxProps, XmlToJsxChildrenGenerator, XmlToJsxCom } from "App/modules/xmlToJsxGen";
import { TypographyProps } from "@material-ui/core/Typography";
import Tag from "App/modules/instruments/widgets/shared/Tag";
import Box from "@material-ui/core/Box/Box";
import Paper from "@material-ui/core/Paper/Paper";
import { InterfaceFilterContext } from "../Integration";

const RootImplementation: React.FC = ({ children }) => {
    return (
        <Paper>
            <Box m={1} p={1}>
                {children}
            </Box>
        </Paper>
    );
};

export const GenericInterfaceComp: XmlToJsxCom = ({ content }) => {
    const interfaceFilterContext = useContext(InterfaceFilterContext);

    useEffect(() => {
        interfaceFilterContext.registerInterface(content.tagName);
    });

    if (!interfaceFilterContext.isInterfaceDisplayed(content.tagName)) {
        return null;
    }

    return (
        <RootImplementation>
            <>{content && content.tagName && <Tag label={content.tagName} />}</>
            {content && <XmlToJsxChildrenGenerator content={content} />}
        </RootImplementation>
    );
};

const Implementation: React.FunctionComponent<IXmlToJsxProps & TypographyProps> = ({ content }) => (
    <XmlToJsxChildrenGenerator content={content} />
);

export default Implementation;
