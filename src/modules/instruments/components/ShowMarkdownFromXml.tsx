import React, { ReactType } from "react";
import ReactMarkdown from "react-markdown";
import Box from "@material-ui/core/Box/Box";
import { ReactMarkdownUtils } from "App/components/reactMarkdown";
import Utilities from "App/services/Utilities";

const ShowMarkdownFromXml: React.FunctionComponent<{
    content: string | null;
    renderers?: { [nodeType: string]: ReactType };
}> = ({ content, renderers }) => (
    <Box paddingTop={1} paddingBottom={1}>
        <ReactMarkdown
            renderers={renderers ? renderers : ReactMarkdownUtils.MDRenderObject}
            source={Utilities.removeWhiteSpaceFromXmlContent(content)}
        />
    </Box>
);

export default ShowMarkdownFromXml;
