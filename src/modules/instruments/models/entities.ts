import { IInstrument, IInstrumentIntegration, IIntegration, IVendor } from "./models";
import { InstrumentsLabel } from "../state";
import { InstrumentActions, InstrumentIntegrationActions, IntegrationActions, VendorActions } from "../ducks";
import { manager, RepoBuilder } from "App/services/orm";

const path = InstrumentsLabel.STATE;

const instrumentsName = "instruments";
const integrationsName = "integrations";
const vendorsName = "vendors";
const instrumentIntegrationName = "instrumentIntegration";

manager.addRepository(RepoBuilder.newBuilder<InstrumentActions, IInstrument>(instrumentsName, path).build());
manager.addRepository(RepoBuilder.newBuilder<IntegrationActions, IIntegration>(integrationsName, path).build());
manager.addRepository(RepoBuilder.newBuilder<VendorActions, IVendor>(vendorsName, path).build());
manager.addRepository(
    RepoBuilder.newBuilder<InstrumentIntegrationActions, IInstrumentIntegration>(instrumentIntegrationName, path)
        .withParents([
            { relatedTo: instrumentsName, fk: "instrumentId" },
            { relatedTo: integrationsName, fk: "integrationId" },
        ])
        .build()
);

export const repoInfo = { path, instrumentsName, integrationsName, instrumentIntegrationName, vendorsName };
