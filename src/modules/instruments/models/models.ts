import { IObjectWithId } from "App/services/orm";
import React from "react";

/**
 * Instruments & Integrations INDEX
 */
export interface IInstrument extends IObjectWithId {
    vendor: string;
    model: string;
    fullName: string;
    filePath: string;
    integrationIds: string[];
}

export interface IIntegration {
    id: string;
    name: string;
    interfaceTypes: Set<string>;
    filePath: string;
}

export interface IVendor {
    id: string;
    name: string;
    contact: string;
    filePath: string;
}

export interface IIndex {
    instruments: IInstrument[];
    integrations: IIntegration[];
    vendors: IVendor[];
}

/**
 * Pivot Instruments <-> Integrations
 */
export interface IInstrumentIntegration extends IObjectWithId {
    instrumentId: string;
    integrationId: string;
}

/**
 * Instruments
 */

type LnxDeps = "";
type WinDeps = ".NET Framework (v4)";
type OsxDeps = "";

type WinSys<T = "Windows"> = {
    os: T;
    dependencies: (T extends "Windows" ? WinDeps : "")[];
};
type OsxSys<T = "Osx"> = {
    os: T;
    dependencies: (T extends "Osx" ? OsxDeps : "")[];
};
type LnxSys<T = "Linux"> = {
    os: T;
    dependencies: (T extends "Linux" ? LnxDeps : "")[];
};
type SoftwareMinReq = WinSys | OsxSys | LnxSys;

interface IInstrumentSoftware {
    name: string;
    detail: string;
    supportedClients?: {
        name: string;
        link: string;
    };
    minReq: SoftwareMinReq;
}
export interface IFullInstrument extends IObjectWithId {
    instrumentName: string;
    description: string;
    purpose: string;
    versions: string[];
    software: IInstrumentSoftware[];
}

/**
 * Integrations
 */
type CoverageStructureType = {
    coverage: string;
    structure: "XML" | "CSV" | "BMP";
    when?: never;
    description?: never;
};
type WhileType = {
    when: string;
    description: string;
    coverage?: never;
    structure?: never;
};
type InterfaceImplementation = {
    interface: "N/A" | "Not supported" | "File: CSV" | "File: XML" | "File: BMP" | "";
    implementation: (string | CoverageStructureType | WhileType)[];
};
type CommandRowType = {
    command: string; // todo: is this extensible? or can we fix with "Method Download" | "..."
    details: InterfaceImplementation[];
};
type CommandSetType = CommandRowType[];
export interface IFullIntegration extends IObjectWithId {
    instrumentName: string;
    instrumentVersion: string;
    type: "SiLA" | "OPC" | "REST" | "instrument-level";
    commandSet: CommandSetType;
}
