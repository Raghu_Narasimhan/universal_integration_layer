import { GetComponentMapperType, XmlToJsxCom, PathMapType, WildCardType } from "./index";

const getXmlPath = (element: Element) => {
    let node: Element | null = element;
    let path = "";
    while (node) {
        const parent = node as Element;
        path = "/" + node.nodeName + path;
        node = parent.parentElement;
    }
    return path;
};

const capitalize = (str: string): string => {
    if (str) {
        const firstChar = str.charAt(0);
        if (firstChar) {
            return firstChar.toUpperCase() + str.slice(1);
        }
    }
    return str;
};

/**
 *
 * @ImplNote: to grab all the children beneath the wildcard, we use the following regex:
 * ^(\/Integration\/Functionality\/Interface\/).+(\/Implementation)$

 * If we want to target all children with some leaf path, we not only aim at one specific level node
 * example: /Integration/Functionality/Interface/blabla/Implementation/More <= true
 * example: /Integration/Functionality/Interface/blabasdasd/sdafasdf/sadfsdaf//Implementation/More <= false
 * ^(\/Integration\/Functionality\/Interface\/).[^\/]+(\/Implementation)$
 */
const getComponentMapper: GetComponentMapperType = (
    pathMap: PathMapType,
    wildCard: WildCardType[],
    queryPath: string,
    exactPath: boolean
): XmlToJsxCom | null => {
    const existent = pathMap.get(queryPath);
    if (existent) {
        return existent;
    } else {
        if (queryPath.endsWith("#text")) {
            return null;
        }
        let result: XmlToJsxCom | null = null;
        wildCard.forEach((item) => {
            const splitPath = item.path.split("*");
            let regex: RegExp = new RegExp(`^(${queryPath})`);

            if (splitPath.length === 0) {
                return;
            } else if (splitPath.length === 1) {
                console.warn("Please only add wildcard paths into wildCard array");
                return;
            } else {
                splitPath.forEach((part, index) => {
                    if (index === 0) {
                        regex = new RegExp(`^(${part})`);
                    } else {
                        if (exactPath) regex = new RegExp(`${regex.source}.[^\\/]+(${part})$`);
                        else regex = new RegExp(`${regex.source}.+(${part})$`);
                    }
                });
            }
            if (queryPath.match(regex)) result = item.component;
        });
        return result;
    }
};

export const XmlToJsxUtilities = {
    capitalize,
    getXmlPath,
    getComponentMapper,
};
