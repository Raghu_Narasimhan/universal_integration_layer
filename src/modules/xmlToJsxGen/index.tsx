import React, { ElementType } from "react";
import { XmlToJsxChildrenGenerator, XmlToJsxTitle } from "./XmlToJsxGenerator";
import { Theme } from "@material-ui/core";

/**
 * Types
 */
export interface IXmlToJsxContext {
    defaultTheme: Theme;
    mapUtilities?: MapUtilities;
    exactPath?: boolean;
    defaultReactMarkdownRenderers?: { [NodeType: string]: ElementType<any> };
    markdownCodePreprocessor?: (content: string) => string;
}

export interface IXmlToJsxProps {
    content: Element;
    style?: React.CSSProperties;
}

export type SetContentVoidFunction = (newContent: React.ReactNodeArray, newDocument: Document) => void;

export type XmlToJsxCom = React.FunctionComponent<IXmlToJsxProps>;

export type WildCardType = { path: string; component: XmlToJsxCom };

export type GetComponentMapperType = (
    pathMap: PathMapType,
    wildCard: WildCardType[],
    path: string,
    newMode: boolean
) => React.FunctionComponent<IXmlToJsxProps> | null;

export type PathMapType = Map<string, XmlToJsxCom>;
export type MapUtilities = { pathMap: PathMapType; wildCard: WildCardType[] };

const EmptyWithChildren: XmlToJsxCom = ({ content }) => <XmlToJsxChildrenGenerator content={content} />;
const Empty: XmlToJsxCom = () => <></>;

export { XmlToJsxChildrenGenerator, XmlToJsxTitle, EmptyWithChildren, Empty };
