export enum AppLabel {
    STATE = "app",
}

export interface IAppState {
    readonly loading: boolean;
    readonly location: {
        previous: string;
        current: string;
    };
}

export const initialState: IAppState = {
    loading: true,
    location: {
        previous: "",
        current: "",
    },
};
