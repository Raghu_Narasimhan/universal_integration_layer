import { Reducer } from "redux";
import { ThunkAction } from "redux-thunk";
import { replace, push, LOCATION_CHANGE } from "connected-react-router";
import { IPayloadAction } from "App/types";
import { IGlobalState } from "App/appstate/store";
import { AppLabel, IAppState, initialState } from "App/appstate/state";

/**
 * Action types
 */
export enum ActionTypes {
    SET_LOADING = "APP/SET_LOADING",
}

interface ISetLoadingAction extends IPayloadAction<ActionTypes.SET_LOADING, boolean> {}
interface IRedirectAction extends IPayloadAction<any, any> {}

export type AppActionsInterface = ISetLoadingAction | IRedirectAction;

/**
 * Concrete Actions
 */
const setLoadingState = (payload: boolean): ISetLoadingAction => ({
    payload,
    type: ActionTypes.SET_LOADING,
});

/**
 * Thunks
 */
type RedirectThunk = (path: string) => ThunkAction<any, IGlobalState, null, IRedirectAction>;

const redirect: RedirectThunk = (path: string) => (dispatch) => {
    dispatch(push(path));
};

const updateRoute: RedirectThunk = (path: string) => (dispatch) => {
    dispatch(replace(path));
};

export const actions = {
    setLoadingState,
    redirect,
    updateRoute,
};

/**
 * Reducer
 */
type AppReducerType = Reducer<IAppState, AppActionsInterface>;

export const AppReducer: AppReducerType = (state: IAppState = initialState, action: AppActionsInterface): IAppState => {
    switch (action.type) {
        case ActionTypes.SET_LOADING:
            return { ...state, loading: action.payload };
        case LOCATION_CHANGE:
            // no change if current location == action location
            const noChange: boolean = state.location.current === action.payload.location.pathname;
            return {
                ...state,
                // if no change, location is old state location, else do new
                location: noChange
                    ? state.location
                    : {
                          previous: state.location.current,
                          current: action.payload.location.pathname,
                      },
            };
        default:
            return state;
    }
};

/**
 * Selectors
 */
const isAppLoading = (state: IGlobalState): boolean => state[AppLabel.STATE].loading;
const previousRoute = (state: IGlobalState): string =>
    state[AppLabel.STATE].location ? state[AppLabel.STATE].location.previous : `${process.env.PUBLIC_URL}`;

export const selectors = {
    isAppLoading,
    previousRoute,
};
