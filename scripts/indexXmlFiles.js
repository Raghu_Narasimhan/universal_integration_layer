const fs = require("fs");
const fastXmlParser = require("fast-xml-parser");

const publicPath = __dirname + "/../public-assets/";
const instrumentsPath = "xml/instruments/";
const integrationsPath = "xml/integrations/";
const vendorsPath = "xml/vendors/";
const instrumentsAbsolutePath = publicPath + instrumentsPath;
const integrationsAbsolutePath = publicPath + integrationsPath;
const vendorsAbsolutePath = publicPath + vendorsPath;

const index = {
    instruments: [],
    integrations: [],
    vendors: []
};

function getInstrumentFullName(instrument) {
    return instrument["Vendor"] + " " + instrument["Model"];
}

function getInstrumentData(instrumentFile) {
    const content = fs.readFileSync(instrumentsAbsolutePath + instrumentFile).toString();
    const json = fastXmlParser.parse(content);
    const instrumentProperties = json["Instrument"]["Properties"];
    return {
        vendor: instrumentProperties["Vendor"],
        model: instrumentProperties["Model"],
        fullName: getInstrumentFullName(instrumentProperties),
        filePath: instrumentsPath + instrumentFile,
        integrationIds: [],
    };
}

function getVendorData(vendorFile) {
    const content = fs.readFileSync(vendorsAbsolutePath + vendorFile).toString();
    const json = fastXmlParser.parse(content);
    return {
        name: json["Vendor"]["Name"],
        contact: json["Vendor"]["Contact"],
    }
}

function getInstruments(integrationFile) {
    const content = fs.readFileSync(integrationFile).toString();
    const json = fastXmlParser.parse(content);
    const instruments = json["Integration"]["SupportedInstrument"];
    return Array.isArray(instruments) ? instruments : [instruments];
}

function getInterfaceTypes(integrationJson) {
    const interfaceTypesResult = [];
    const integration = integrationJson["Integration"];
    const name = integration["Name"];
    if (integration) {
        const functionalities = integration["Functionality"];
        if (functionalities) {
            Array.from(functionalities).forEach((functionality) => {
                const interfaceObject = functionality["Interface"];
                if (interfaceObject) {
                    Object.keys(interfaceObject).forEach((item) => {
                        if (isNaN(item)) {
                            interfaceTypesResult.push(item);
                        } else {
                            // one level deeper!
                            const newItem = Object.keys(interfaceObject[item])[0];
                            interfaceTypesResult.push(newItem); // the next key should be only one :)
                        }
                    });
                }
            });
        }
    }
    return { name, interfaceTypes: [...new Set(interfaceTypesResult)] }; // sets are not serialise
}

fs.readdirSync(vendorsAbsolutePath).forEach(function(vendorFile) {
    index.vendors.push(getVendorData(vendorFile));
});

fs.readdirSync(instrumentsAbsolutePath).forEach(function(instrumentFile) {
    const instrumentData = getInstrumentData(instrumentFile);
    if (index.vendors.findIndex(v => v.name === instrumentData.vendor) === -1) {
        throw Error("Vendor " + instrumentData.vendor + " doesn't exist!");
    }
    index.instruments.push(instrumentData);
});

fs.readdirSync(integrationsAbsolutePath).forEach(function(integrationFile, i) {
    const id = i.toString();
    const integrationFileContent = fs.readFileSync(integrationsAbsolutePath + integrationFile).toString();
    const integrationJson = fastXmlParser.parse(integrationFileContent);
    const vendor = integrationJson["Integration"]["Vendor"];
    if (index.vendors.findIndex(v => v.name === vendor) === -1) {
        throw Error("Vendor " + vendor + " doesn't exist!");
    }

    const { name, interfaceTypes } = getInterfaceTypes(integrationJson);
    // add to integrations
    index.integrations.push({
        id: id,
        name,
        interfaceTypes,
        filePath: integrationsPath + integrationFile
    });
    // add to instruments
    const instruments = getInstruments(integrationsAbsolutePath + integrationFile);
    instruments.forEach((instrument) => {
        const instrumentIndex = index.instruments.find((idx) => idx["fullName"] === getInstrumentFullName(instrument));
        if (instrumentIndex) {
            instrumentIndex.integrationIds.push(id);
        } else {
            throw Error("Instrument " + getInstrumentFullName(instrument) + " doesn't exist!");
        }
    });
});

fs.writeFileSync(publicPath + "index.json", JSON.stringify(index, null, 4));
